(TeX-add-style-hook
 "main"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("beamer" "xcolor={table}" "compress")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("ulem" "normalem") ("babel" "greek" "spanish" "english") ("fontspec" "no-math") ("biblatex" "backend=biber" "style=authoryear-icomp" "natbib=true" "url=false" "doi=false" "isbn=false" "eprint=false")))
   (add-to-list 'LaTeX-verbatim-environments-local "semiverbatim")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "beamer"
    "beamer10"
    "color"
    "ulem"
    "babel"
    "fontspec"
    "xunicode"
    "graphicx"
    "fixltx2e"
    "array"
    "amsmath"
    "unicode-math"
    "booktabs"
    "linguex"
    "forest"
    "avm"
    "enumitem"
    "biblatex")
   (TeX-add-symbols
    '("toverline" 1)
    "mhyphen"
    "wlist")
   (LaTeX-add-xcolor-definecolors
    "dark-gray"
    "242gray"
    "titleblue"
    "titlered"
    "titlegreen"
    "titletextblue"
    "titletextred"
    "igrablue1"
    "igrablue2"
    "igrablue3"
    "igrablue"
    "igrabluedark"
    "igragreen"
    "igragreendark"))
 :latex)

